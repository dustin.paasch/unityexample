using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthText : MonoBehaviour
{
    [SerializeField]
    private Damageable _dmgComponent;

    void Start()
    {
        this._dmgComponent.OnDamage += UpdateText;
        this._dmgComponent.OnDeath += DeathMessage;
    }

    void UpdateText(int newHealth)
    {
        this.gameObject.GetComponent<Text>().text = newHealth.ToString();
    }

    void DeathMessage()
    {
        this.gameObject.GetComponent<Text>().text = "U DED";
    }
}
