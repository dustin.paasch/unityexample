using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Damageable))]
public class Enemy : MonoBehaviour
{
   private Damageable _dmgComponent;


   void Start()
   {
       this._dmgComponent = this.GetComponent<Damageable>();
   }
}
