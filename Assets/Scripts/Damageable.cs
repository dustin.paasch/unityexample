using System;
using UnityEngine;
using UnityEngine.UI;

public class Damageable : MonoBehaviour
{
    [SerializeField]
    private int _health = 100;

    public bool IsAlive { private set; get; } = true;

    public delegate void DamageDelegate(int newHealth);
    public event DamageDelegate OnDamage;

    public event Action OnDeath;


    public void ApplyDamage(int amount)
    {
        this._health -= amount;

        if (this._health < 1)
        {
            this._health = 0;
            this.IsAlive = false;
            this.OnDeath?.Invoke();
        }
        else
        {
            this.OnDamage?.Invoke(this._health);
            GameObject.Destroy(this.gameObject);
        }
    }
}
