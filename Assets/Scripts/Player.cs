using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Movable))]
public class Player : MonoBehaviour
{
    private Movable _movable;
    private List<PowerUp> _powerUpList;

    [SerializeField]
    private GameObject _projectilePrefab;

    [SerializeField]
    private float _projectileSpawnHover;

    public Vector3 AimPosition;


    void Start()
    {
        this._powerUpList = new List<PowerUp>();
        this._movable = this.GetComponent<Movable>();
    }

    public void Fire()
    {
        // Spawn new projectile.
        GameObject projectileObject = GameObject.Instantiate(this._projectilePrefab);
        Projectile projectileComponent = projectileObject.GetComponent<Projectile>();

        if (projectileObject == null)
            throw new Exception("Prefab is missing Projectile component!");

        Vector3 look = this.AimPosition - this.gameObject.transform.position;
        Vector3 direction = new Vector3(look.x, 0, look.z);
        Quaternion lookQuat = Quaternion.LookRotation(direction);
        projectileObject.transform.rotation = lookQuat;
        
        projectileObject.transform.position = this.gameObject.transform.position + Vector3.up * this._projectileSpawnHover;
    }
}
