using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Movable : MonoBehaviour
{
    [SerializeField]
    private float _speed = 10.0f;
    private Rigidbody _rigidBody;
    public Vector3 IntendedVelocity;

    void Awake()
    {
        this._rigidBody = this.gameObject.GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        this._rigidBody.velocity = this.IntendedVelocity.normalized * this._speed * Time.fixedDeltaTime;
    }
}
