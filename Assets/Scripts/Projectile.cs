using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private int _damage = 5;

    [SerializeField]
    private float _speed = 100f;

    [SerializeField]
    private float _lifetime = 1f;
    private float _elapsedLifetime = 0f;


    void FixedUpdate()
    {
        this.gameObject.transform.position += this.gameObject.transform.forward * Time.fixedDeltaTime * this._speed;
    }


    void Update()
    {
        this._elapsedLifetime += Time.deltaTime;

        if (this._elapsedLifetime >= this._lifetime)
            GameObject.Destroy(this.gameObject);
    }

    void OnTriggerEnter(Collider collider)
    {
        GameObject.Destroy(this.gameObject);
        Damageable dmgComponent = collider.gameObject.GetComponent<Damageable>();

        if (dmgComponent == null)
            return;

        dmgComponent.ApplyDamage(this._damage);
    }
}
