using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Movable), typeof(Player))]
public class PlayerController : MonoBehaviour
{
    public delegate void PossessedChangedDelegate(GameObject gameObject);
    public event PossessedChangedDelegate PossessedChanged;

    private Movable _moveable;
    private Player _player;

    private InputMap _inputMap;


    void Start()
    {
        this._inputMap = new InputMap();
        this._inputMap.Enable();
        this._inputMap.PlayerControls.Fire.performed += Fire;

        this._moveable = this.gameObject.GetComponent<Movable>();
        this._player = this.gameObject.GetComponent<Player>();
    }


    // Update is called once per frame
    void Update()
    {
        Vector2 intendedMovement = this._inputMap.PlayerControls.Movement.ReadValue<Vector2>();
        this._moveable.IntendedVelocity = new Vector3(intendedMovement.x, 0, intendedMovement.y);

        // Fetch mouse position on screen.
        Vector2 mousePosition = Mouse.current.position.ReadValue();
        Camera camera = Camera.main;

        // Raycast from mouse position on screen to world space.
        Ray ray = camera.ScreenPointToRay(new Vector3(mousePosition.x, mousePosition.y, camera.nearClipPlane));

        // Extend the ray to the origin plane (zero plane)
        Vector3 targetPosition = ray.origin - (ray.direction * Vector3.Dot(ray.origin, Vector3.up) / Vector3.Dot(ray.direction, Vector3.up));

        this._player.AimPosition = targetPosition;
    }


    void Fire(InputAction.CallbackContext _)
    {
        this._player.Fire();
    }
}
