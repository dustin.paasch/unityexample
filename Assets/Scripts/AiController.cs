using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Movable))]
public class AiController : MonoBehaviour
{
    private Movable _movable;
    private GameObject _chaseTarget;


    void Start()
    {
        this._movable = this.GetComponent<Movable>();
    }
}
