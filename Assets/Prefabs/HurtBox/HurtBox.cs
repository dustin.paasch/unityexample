using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour
{
    [SerializeField]
    private int _damage = 10;

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log(collider.gameObject.name);

        Damageable dmgComponent = collider.gameObject.GetComponent<Damageable>();

        dmgComponent?.ApplyDamage(this._damage);

    }
}
